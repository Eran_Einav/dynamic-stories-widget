# Dynamic Stories Widget

Development environment for dynamic stories widget, based on Angular, Node.js & Express.js.

## Getting Started

Getting started with the project.
Open a terminal and navigate to where you want to clone the repository to, then run the following command:

```
git clone https://Eran_Einav@bitbucket.org/Eran_Einav/dynamic-stories-widget.git
cd dynamic-stories-widget
```
The repository will be cloned into a new directory with name of "dynamic-stories-widget".
Then we are using `cd` command for changing the current working directory into the new "dynamic-stories-widget" directory, for the next steps.

### Prerequisites

1. Node.js and npm: You should have already Node.js and npm installed on your machine.
If you doesn't, please refer to this page: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
2. Chrome browser: We are using Chrome browser in the npm scripts but you can use an alternative browser if you prefer foxes ;)

### Installing

The project is depends on npm packages and we can't start without it.
Back to the terminal, Assuming you are already in the root of the project, run the following command:

```
npm run install-and-run-all
```
It should install all the dependencies of the project at once so it may take a while.
After the installation process the servers will start and the demo page should be open automatically in your Chrome browser, assuming Chrome is already installed on your machine.
If Chrome is not installed and you prefer to use an alternative browser: after the installation process run the following command: `npm run start-all`, when the servers will come up you will see in the terminal the url address you should use, run it in your browser.

The widget should be loading into the demo web page, appearing after couple of seconds.

You can modify the design of the widget by passing different values to the attributes of the target inject element.
From the root, go to demo/website/index.html and edit the file, you will see a demonstration of the available options and comments for explanation.

## Authors

*Eran Einav*
