(function(d, w){

    function Widget(){

        /*
         * getWidget(): get the widget markup from server via AJAX GET request, send the response to a callback.
         */
        this.getWidget = function(url, cb){

            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    cb(this.responseText);
                }
            };
            xhttp.open("GET", url, true);
            xhttp.send();

        }

        /*
         * attrToURLParams(): convert element's attributes into parmeters for server request, merge it with the request url. 
         */
        this.attrToURLParams = function($ele, url){

            var consumer_id = '0',
            params = '?';
        
            for (var i = 0; i < $ele.attributes.length; i++){
                if (typeof $ele.attributes[i] === 'object' && $ele.attributes[i].name.indexOf('data-') > -1){
                    var val = $ele.attributes[i].value;
                    if (!val.length) continue;
                    params += $ele.attributes[i].name + '=' + encodeURIComponent(val);
                    if ($ele.attributes[i].name == 'data-consumer-id') consumer_id = val;
                    if ((i + 1) != $ele.attributes.length) params += '&';
                }
            }

            if (consumer_id == '0'){
                $ele.remove();
                console.error('data-plugin.js error: abort! please supply consumer id!');
                return false;
            }

            url += consumer_id + params;

            return url;

        }

        /*
         * onInit(): initial process.
         */
        this.onInit = function(){

            var $ele = d.querySelector('#suggested-stories-container'),
            url = 'http://localhost:3000/api/widget/',
            url = this.attrToURLParams($ele, url);

            if (!url) return;

            this.getWidget(url, function(res){

                var res_obj = JSON.parse(res);
                $ele.innerHTML = res_obj.markup;

            });

        }

    }

    /*
     * After DOM is ready send request to the server to get the widget's markup.
     * We send the inject target element's attributes as parameters to server for custom response.
     * After parsing the response data inject the markup into the target element.
     */
    w.addEventListener('DOMContentLoaded', function(ev){

        new Widget().onInit();

    });

})(document, window);

