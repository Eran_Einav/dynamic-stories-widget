const handler = require('serve-handler');
const http = require('http');
const port = 5000;

const server = http.createServer((request, response) => {
  return handler(request, response);
})
 
server.listen(port, () => {
  console.log('Running at http://localhost:' + port);
});