var fs = require('fs');

/*
 * This module exports functions for copying files.
 */
module.exports = {
    
    /*
     * Copy file asynchronously.
     * Receive an object with file source path and file destination path.
     * Callback returns error on failure or success message on success.
     */
    copyFileAsync: function(obj) {

        fs.copyFile(obj.src, obj.dest, function(err){

            if (err){
                console.error(obj.src + ' copy error: ' + err);
            }
            else {
                console.log(obj.src + ' was copied to ' + obj.dest);         
            }

        });

    },

    /*
     * Copy files asynchronously. looping through copyFileAsync().
     * Receive an array of objects with file source path and file destination path.
     */
    copyMultiFilesASync: function(files_arr) {

        for (i in files_arr) {
            this.copyFileAsync(files_arr[i]);
        }

    }

}