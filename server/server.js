if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

var express = require('express'),
app = express(),
port = process.env.SERVER_PORT,
functions = require('./functions');

/*
 * Set Express to listen on port 3000.
 */
app.listen(port, function(){

    console.log('Listening on port ' + port);

});


/*
 * Enable Cross Origin Requests.
 */
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});



/*
 * Route for home, right now it doesn't do anything but showing that the server is up.
 */
app.get('/', function (req, res) {
    res.status(200).send('I\'m listening...');
});



/*
 * Route for serving the stories data file.
 * Check if the file exists, if not, make request to get the data and write it to a file for later client requests.
 */
app.get('/api/data/:consumer_id', function (req, res) {

    var params = req.query,
    consumer_id = req.params.consumer_id,
    consumer_dir = process.env.CONSUMER_BASE_DIR + consumer_id + '/',
    params_str = functions.getQueryString(params);

    if (functions.isFileExists(consumer_dir + process.env.STORIES_DATA_FILE)){

        functions.resFileContent(consumer_dir + process.env.STORIES_DATA_FILE, req, res);

    }
    else {

        functions.getFile(process.env.STORIES_DATA_REMOTE_URL + params_str, function(stories){
            functions.WriteFile(consumer_dir + process.env.STORIES_DATA_FILE, stories);
             functions.resFileContent(consumer_dir + process.env.STORIES_DATA_FILE, req, res);
        });

    }
    
});



/*
 * Route for serving the widget's markup file.
 * Check if the file exists, if not, make request to get the widget's markup and write it to a file for later client requests.
 * Since the widget's markup depends on the stories data so we first check if the stories data file exists, if not, make request to get the data and write it to a file.
 * Get the parameters from the client request and pass the widget's design attributes to the Angular app so we will get the widget's markup with the custom design of the consumer.
 */
app.get('/api/widget/:consumer_id', function (req, res) {

    var params = req.query,
    consumer_id = req.params.consumer_id,
    consumer_dir = process.env.CONSUMER_BASE_DIR + consumer_id + '/',
    params_str = functions.getQueryString(params),
    prev_params = '';


    if (functions.isFileExists(consumer_dir + process.env.PARAMS_DATA_FILE)){

        prev_params = functions.readFileContentSync(consumer_dir + process.env.PARAMS_DATA_FILE);

        if (prev_params == params_str){

            if (functions.isFileExists(consumer_dir + process.env.WIDGET_FILE)){

                functions.resFileContent(consumer_dir + process.env.WIDGET_FILE, req, res);

            }
            else {

                if (functions.isFileExists(consumer_dir + process.env.STORIES_DATA_FILE)){

                    functions.scrapePage(process.env.ANGULAR_STORIES_ROUTE_URL + '/' + consumer_id + '/' + params_str, function(content){
                        functions.cutMarkupAndWriteFile(content, consumer_dir + process.env.WIDGET_FILE, true);
                        functions.resFileContent(consumer_dir + process.env.WIDGET_FILE, req, res);
                    });

                }
                else {

                    functions.getFile(process.env.STORIES_DATA_REMOTE_URL + params_str, function(stories){

                        functions.WriteFile(consumer_dir + process.env.STORIES_DATA_FILE, stories);

                        functions.scrapePage(process.env.ANGULAR_STORIES_ROUTE_URL + '/' + consumer_id + '/' + params_str, function(content){
                            functions.cutMarkupAndWriteFile(content, consumer_dir + process.env.WIDGET_FILE, true);
                            functions.resFileContent(consumer_dir + process.env.WIDGET_FILE, req, res);
                        });

                    });

                }

            }

        }
        else {

            functions.WriteFile(consumer_dir + process.env.PARAMS_DATA_FILE, params_str);

            if (functions.isFileExists(consumer_dir + process.env.STORIES_DATA_FILE)){

                functions.scrapePage(process.env.ANGULAR_STORIES_ROUTE_URL + '/' + consumer_id + '/' + params_str, function(content){
                    functions.cutMarkupAndWriteFile(content, consumer_dir + process.env.WIDGET_FILE, true);
                    functions.resFileContent(consumer_dir + process.env.WIDGET_FILE, req, res);
                });

            }
            else {

                functions.getFile(process.env.STORIES_DATA_REMOTE_URL + params_str, function(stories){

                    functions.WriteFile(consumer_dir + process.env.STORIES_DATA_FILE, stories);

                    functions.scrapePage(process.env.ANGULAR_STORIES_ROUTE_URL + '/' + consumer_id + '/' + params_str, function(content){
                        functions.cutMarkupAndWriteFile(content, consumer_dir + process.env.WIDGET_FILE, true);
                        functions.resFileContent(consumer_dir + process.env.WIDGET_FILE, req, res);
                    });

                });

            }

        }

    }
    else {

        functions.WriteFile(consumer_dir + process.env.PARAMS_DATA_FILE, params_str);

        if (functions.isFileExists(consumer_dir + process.env.WIDGET_FILE)){

            functions.resFileContent(consumer_dir + process.env.WIDGET_FILE, req, res);

        }
        else {

            functions.getFile(process.env.STORIES_DATA_REMOTE_URL + params_str, function(stories){

                functions.WriteFile(consumer_dir + process.env.STORIES_DATA_FILE, stories);

                functions.scrapePage(process.env.ANGULAR_STORIES_ROUTE_URL + '/' + consumer_id + '/' + params_str, function(content){
                    functions.cutMarkupAndWriteFile(content, consumer_dir + process.env.WIDGET_FILE, true);
                    functions.resFileContent(consumer_dir + process.env.WIDGET_FILE, req, res);
                });

            });

        }

    }

});