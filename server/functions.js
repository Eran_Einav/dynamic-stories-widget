if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

var fs = require("fs"),
path = require('path'),
https = require('https'),
puppeteer = require('puppeteer'),
minify = require('html-minifier').minify,
CleanCSS = require('clean-css');

/*
 * This module exports functions for data manipulation. 
 */
module.exports = {
    
    /*
     * Get query string for object of parameters.
     */
    getQueryString: function(params, encode = true){

        var c = 0,
        params_str = '?';

        for (i in params){
            c++;
            var val = (encode) ? encodeURIComponent(params[i]) : params[i];
            params_str += i + '=' + val;
            if (c != Object.keys(params).length) params_str += '&';
        }

        return params_str;

    },

    /*
     * Check if file exists, return callbacks.
     */
     isFileExistsCB: function(file, ex_cb, not_ex_cb){

        try {

            if (fs.existsSync(file)) {
                //file exists
                return ex_cb();
            }
            else {
                return not_ex_cb();
            }

        } catch(err) {

            console.error('Error:', err);

        }

    },

    /*
     * Check if file exists, return boolean.
     */
     isFileExists: function(file){

        try {

            if (fs.existsSync(file)) {
                //file exists
                return true;
            }
            else {
                return false;
            }

        } catch(err) {

            console.error('Error:', err);

        }

    },

    /*
     * Cut widget's markup and write it to a file.
     */
    cutMarkupAndWriteFile: function(content, path, is_minify = true){

        if (is_minify){
            style = new CleanCSS({}).minify(content).styles;
            style = '<style>' + style + '</style>';
        }

        s = content.indexOf('<app-stories');
        e = content.indexOf('</app-root>');
        content = style + content.substring(s, e);

        return this.WriteFile(path, content);

    },

    /*
     * scrape page.
     */
    scrapePage: function(url, cb){

        (async function(){

          var browser = await puppeteer.launch(),
          page = await browser.newPage();
          await page.goto(url);
          var content = await page.content();
          await browser.close();

          cb(content);

        })();

    },

    /*
     * Write file.
     */
    WriteFile: function(path, data){

        var dir = path.substring(0, path.lastIndexOf("/"));

        if (!fs.existsSync(process.env.CONSUMER_BASE_DIR)){
            fs.mkdirSync(process.env.CONSUMER_BASE_DIR);
        }

        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }

        fs.writeFileSync(path, data); 

    },

    /*
     * Get file from third party.
     */
    getFile: function(url, cb){

        https.get(url, function(response){
            
            let stories = '';

            // called when a data chunk is received.
            response.on('data', function(chunk){
                stories += chunk;
            });

            // called when the complete response is received.
            response.on('end', function(){
                 
                cb(stories);

            });

        }).on("error", function(error){
            console.log("https.get() error: " + error.message);
        });

    },

    /*
     * Read file content, async.
     */
    readFileContent: function(file, cb){

        var filePath = path.join(__dirname, file);

        fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){

            if (!err) {
                return cb(data);
            } else {
                return console.log(err);
            }

        });

    },

    /*
     * Read file content, sync.
     */
    readFileContentSync: function(file, cb){

        var filePath = path.join(__dirname, file);

        fs.readFileSync(filePath, 'utf-8', function(err, data){

            if (!err) {
                return data;
            } else {
                return console.log(err);
            }

        });

    },

    /*
     * Response file content.
     */
    resFileContent: function(file, req, res){

        this.readFileContent(file, function(data){

            var status = 200,
            data_str = JSON.stringify({
                status: status,
                markup: data
            });
            res.writeHead(status, {'Content-Type': 'text/html'});
            res.write(data_str);
            res.end();

        });

    }

};