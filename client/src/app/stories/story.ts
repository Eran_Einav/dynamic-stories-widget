export class Story {
  id: number;
  publish_date: string;
  cover_image_url: string;
  cover_thumb_url: number;
  story_title: string;
  story_sub_title: string;
  trends_detected: number;
  language: string;
  link: string;
}