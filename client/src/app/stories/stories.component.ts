import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { StoryService } from './story.service';
import { Story } from './story';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss']
})
export class StoriesComponent implements OnInit {

    assets_img_dir: string = 'http://localhost:4200/assets/img/';
    stories: Story[];
    consumer_id: string;
    bg_color: string;
    title_color: string;
    sub_title_color: string;
    button_color: string;

    constructor(private storyService: StoryService, private route: ActivatedRoute){}

    getStories(consumer_id): void {
        this.storyService.getStories(consumer_id)
        .subscribe(
            (data)=>{
                this.stories = JSON.parse(data.markup).data.stories;
            }
        );
    }

    ngOnInit(){
        this.consumer_id = this.route.snapshot.paramMap.get('consumer_id');
        this.storyService.setConsumerId(this.consumer_id);
        this.route.queryParams.subscribe(params => {
            this.bg_color = params['data-bg-color'];
            console.log(params)
            this.title_color = params['data-title-color'];
            this.sub_title_color = params['data-sub-title-color'];
            this.button_color = params['data-button-color'];
            this.getStories(this.consumer_id);

        });

    }

}