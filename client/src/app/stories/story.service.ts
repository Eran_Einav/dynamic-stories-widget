import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
 
import { Story } from './story';
 
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable({ providedIn: 'root' })
export class StoryService {

  private consumer_id: string;
  private url = 'http://localhost:3000/api/data/';

  constructor(private http:HttpClient) {}

  getStories(consumer_id): Observable<any> {
    console.log('consumer_id', consumer_id)
    this.http.get(this.url + consumer_id, {responseType: 'text'})
    .subscribe(data => data);
    return this.http.get<any>(this.url + consumer_id);
  }

  setConsumerId(consumer_id): void {
    this.consumer_id = consumer_id;
  }

}



